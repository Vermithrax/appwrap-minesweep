/**
 * Copyright (c) 2017
 * AppWrap LLC
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of AppWrap LLC. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with AppWrap LLC.
 *
 * Script Name: AppWrap | SL - SL Creator Dashboard
 *
 * Script Description:
 * Automatically creates a suitelet based on the 'Appwrap Suitelet Creator' custom record.
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *     | Author                      | Date          | Version       | Comments                                                                |
 *     |-----------------------------|---------------|---------------|-------------------------------------------------------------------------|
 *     | J.Cuanan                    | Nov 2017      | 1.0           | SL Creator Dashboard - Initial Version                                  |
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *
 */

/**
 * @NApiVersion 2.0
 * @NModuleScope SameAccount
 * @NScriptName AppWrap | SL - SL Creator Dashboard
 * @NScriptId _sl_sl_creator_dashboard
 * @NScriptType suitelet
 */
define(['N/record', 'N/search', 'N/ui/serverWidget', 'N/runtime',  'N/xml', 'N/error', 'N/file', 'N/render', 'N/log', './appwrap_UtilityFunctions.js'], 
		
function(record, search, serverWidget, runtime, xml, error, file, render, log)
{
	var objURLS = {};
	
	function sl_slCreatorDashboard(option)
	{
		var stLogTitle = 'sl_slCreatorDashboard';
		
		try
		{
			log.debug(stLogTitle, 'START');
			
			var request = option.request;
			var objFormVals = {};
			objFormVals['portlet'] = request.parameters.portlet;
			
			var objDeploymentRecs = search.create({
		        type: 'scriptdeployment',
		        columns: ['internalid',
		                  'script',
		                  'title'],
		        filters: [
		        {
		            name: 'script',
		            operator: 'anyof',
		            values: ['575']
		        }]
		    }).run();

		    var objDeploymentRecsResult = objDeploymentRecs.getRange({
		        start: 0,
		        end: 1000
		    });

		    if (objDeploymentRecsResult)
		    {
		    	for (var idx in objDeploymentRecsResult)
		        {
		    		try
		        	{
		        		var stDeploymentId = objDeploymentRecsResult[idx].getValue('internalid');
		        		var stScriptId = objDeploymentRecsResult[idx].getValue('script');
		        		var stDeploymentTitle = objDeploymentRecsResult[idx].getValue('title');
		        		var stDeploymentCountId = record.load({
		        			type: 'scriptdeployment',
		                	id: stDeploymentId
		        		}).getValue('deploymentid');
		        		
		        		var stURL = 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=' + stScriptId + '&deploy=' + stDeploymentCountId;
		        		objURLS[stDeploymentTitle] = stURL;
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    
		    var objBPARecs = search.create({
		        type: 'customrecord_bpa',
		        columns: ['internalid',
		                  'name'],
		        filters: []
		    }).run();

		    var objBPARecsResult = objBPARecs.getRange({
		        start: 0,
		        end: 1000
		    });

		    var stBPAHTML = '<select name="filters_bpa_1" id="filters_bpa_1" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navhigh\'); filterResults(\'high\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navhigh\');" multiple="" size="4">';
		    stBPAHTML += '<option value="@ALL@">- All -</option>';
		    stBPAHTML += '<option value="@NONE@">- None -</option>';
		    if (objBPARecsResult)
		    {
		    	for (var idx in objBPARecsResult)
		        {
		    		try
		        	{
		        		var stId = objBPARecsResult[idx].getValue('internalid');
		        		var stName = objBPARecsResult[idx].getValue('name');
		        		
		        		stBPAHTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stBPAHTML += '</select>';
		    
		    var stBPA2HTML = '<select name="filters_bpa_2" id="filters_bpa_2" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navmedium\');  filterResults(\'medium\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navmedium\');" multiple="" size="4">';
		    stBPA2HTML += '<option value="@ALL@">- All -</option>';
		    stBPA2HTML += '<option value="@NONE@">- None -</option>';
		    if (objBPARecsResult)
		    {
		    	for (var idx in objBPARecsResult)
		        {
		    		try
		        	{
		        		var stId = objBPARecsResult[idx].getValue('internalid');
		        		var stName = objBPARecsResult[idx].getValue('name');
		        		
		        		stBPA2HTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stBPA2HTML += '</select>';
		    
		    var stBPA3HTML = '<select name="filters_bpa_3" id="filters_bpa_3" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navlow\');filterResults(\'low\'); if(!this.noslaving) { setWindowChanged(window, true); } hideLoadScreen(\'navlow\');" multiple="" size="4">';
		    stBPA3HTML += '<option value="@ALL@">- All -</option>';
		    stBPA3HTML += '<option value="@NONE@">- None -</option>';
		    if (objBPARecsResult)
		    {
		    	for (var idx in objBPARecsResult)
		        {
		    		try
		        	{
		        		var stId = objBPARecsResult[idx].getValue('internalid');
		        		var stName = objBPARecsResult[idx].getValue('name');
		        		
		        		stBPA3HTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stBPA3HTML += '</select>';
		    
		    var objFocusRecs = search.create({
		        type: 'customrecord_focus',
		        columns: ['internalid',
		                  'name'],
		        filters: []
		    }).run();

		    var objFocusRecsResult = objFocusRecs.getRange({
		        start: 0,
		        end: 1000
		    });

		    var stFocusHTML = '<select name="filters_foc_1" id="filters_foc_1" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navhigh\'); filterResults(\'high\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navhigh\');" multiple="" size="4">';
		    stFocusHTML += '<option value="@ALL@">- All -</option>';
		    stFocusHTML += '<option value="@NONE@">- None -</option>';
		    if (objFocusRecsResult)
		    {
		    	for (var idx in objFocusRecsResult)
		        {
		    		try
		        	{
		        		var stId = objFocusRecsResult[idx].getValue('internalid');
		        		var stName = objFocusRecsResult[idx].getValue('name');
		        		
		        		stFocusHTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stFocusHTML += '</select>';
		    
		    var stFocus2HTML = '<select name="filters_foc_2" id="filters_foc_2" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navmedium\'); filterResults(\'medium\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navmedium\');" multiple="" size="4">';
		    stFocus2HTML += '<option value="@ALL@">- All -</option>';
		    stFocus2HTML += '<option value="@NONE@">- None -</option>';
		    if (objFocusRecsResult)
		    {
		    	for (var idx in objFocusRecsResult)
		        {
		    		try
		        	{
		        		var stId = objFocusRecsResult[idx].getValue('internalid');
		        		var stName = objFocusRecsResult[idx].getValue('name');
		        		
		        		stFocus2HTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stFocus2HTML += '</select>';
		    
		    var stFocus3HTML = '<select name="filters_foc_3" id="filters_foc_3" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navlow\'); filterResults(\'low\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navlow\');" multiple="" size="4">';
		    stFocus3HTML += '<option value="@ALL@">- All -</option>';
		    stFocus3HTML += '<option value="@NONE@">- None -</option>';
		    if (objFocusRecsResult)
		    {
		    	for (var idx in objFocusRecsResult)
		        {
		    		try
		        	{
		        		var stId = objFocusRecsResult[idx].getValue('internalid');
		        		var stName = objFocusRecsResult[idx].getValue('name');
		        		
		        		stFocus3HTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stFocus3HTML += '</select>';
		    
		    var objRefSchedRecs = search.create({
		        type: 'customrecord_ref_sch',
		        columns: ['internalid',
		                  'name'],
		        filters: []
		    }).run();

		    var objRefSchedRecsResult = objRefSchedRecs.getRange({
		        start: 0,
		        end: 1000
		    });

		    var stRefSchedHTML = '<select name="filters_rs_1" id="filters_rs_1" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navhigh\'); filterResults(\'high\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navhigh\');" multiple="" size="4">';
		    stRefSchedHTML += '<option value="@ALL@">- All -</option>';
		    stRefSchedHTML += '<option value="@NONE@">- None -</option>';
		    if (objRefSchedRecsResult)
		    {
		    	for (var idx in objRefSchedRecsResult)
		        {
		    		try
		        	{
		        		var stId = objRefSchedRecsResult[idx].getValue('internalid');
		        		var stName = objRefSchedRecsResult[idx].getValue('name');
		        		
		        		stRefSchedHTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stRefSchedHTML += '</select>';
		    
		    var stRefSched2HTML = '<select name="filters_rs_2" id="filters_rs_2" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navmedium\'); filterResults(\'medium\'); if(!this.noslaving) { setWindowChanged(window, true); }hideLoadScreen(\'navmedium\');" multiple="" size="4">';
		    stRefSched2HTML += '<option value="@ALL@">- All -</option>';
		    stRefSched2HTML += '<option value="@NONE@">- None -</option>';
		    if (objRefSchedRecsResult)
		    {
		    	for (var idx in objRefSchedRecsResult)
		        {
		    		try
		        	{
		        		var stId = objRefSchedRecsResult[idx].getValue('internalid');
		        		var stName = objRefSchedRecsResult[idx].getValue('name');
		        		
		        		stRefSched2HTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stRefSched2HTML += '</select>';
		    
		    var stRefSched3HTML = '<select name="filters_rs_3" id="filters_rs_3" class="input" flags="17179873280" type="select-multiple" valuewhenrendered="" aria-labelledby="CUSTRECORD_SUITELET_RS_fs_lbl" onchange="showLoadScreen(\'navlow\'); filterResults(\'low\'); if(!this.noslaving) { setWindowChanged(window, true); } hideLoadScreen(\'navlow\');" multiple="" size="4">';
		    stRefSched3HTML += '<option value="@ALL@">- All -</option>';
		    stRefSched3HTML += '<option value="@NONE@">- None -</option>';
		    if (objRefSchedRecsResult)
		    {
		    	for (var idx in objRefSchedRecsResult)
		        {
		    		try
		        	{
		        		var stId = objRefSchedRecsResult[idx].getValue('internalid');
		        		var stName = objRefSchedRecsResult[idx].getValue('name');
		        		
		        		stRefSched3HTML += '<option value="' + stId + '">' + stName + '</option>';
		        	}
		    		catch (ex)
		    		{}
		        }
		    }
		    stRefSched3HTML += '</select>';
			
			var objForm = serverWidget.createForm({
				title: 'Searches Dashboard',
				hideNavBar : false
			});
			
//			objForm.clientScriptFileId = '../Suitelet Creator/AppWrap_CS_SuiteletCreatorValidator.js';
			
			var objHTML = file.load({
				id: 6106
			});
			
			var objSorttable = file.load({
				id: 6107
			});
			
			var objTest = file.load({
				id: 6108
			});

			var stHTML = objTest.getContents();
			stHTML = stHTML.replace(/{{bpa_1}}/gi, stBPAHTML);
			stHTML = stHTML.replace(/{{foc_1}}/gi, stFocusHTML);
			stHTML = stHTML.replace(/{{rf_1}}/gi, stRefSchedHTML);
			stHTML = stHTML.replace(/{{bpa_2}}/gi, stBPA2HTML);
			stHTML = stHTML.replace(/{{foc_2}}/gi, stFocus2HTML);
			stHTML = stHTML.replace(/{{rf_2}}/gi, stRefSched2HTML);
			stHTML = stHTML.replace(/{{bpa_3}}/gi, stBPA3HTML);
			stHTML = stHTML.replace(/{{foc_3}}/gi, stFocus3HTML);
			stHTML = stHTML.replace(/{{rf_3}}/gi, stRefSched3HTML);
			stHTML += '<script>' + objSorttable.getContents() + '</script>';
			stHTML = doHighAndImmediate(stHTML, objFormVals);
			stHTML = doMedium(stHTML, objFormVals);
			stHTML = doLow(stHTML, objFormVals);
			
			// event handler on to what page/action will be shown/executed
			var objFldAction = objForm.addField({
				id : 'custpage_action',
				type : serverWidget.FieldType.INLINEHTML,
				label : 'Action'
			});
			
			objFldAction.defaultValue = stHTML;
			
			var style = "height: 100%;width: 0; position: fixed;z-index: 100; top: 0;left: 0;background-color: rgb(0,0,0);background-color: rgba(0,0,0, 0.2);overflow-x: hidden; overflow-y:hidden;";
			var str = "";

			var loadScreen = objForm.addField({
				id : 'custpage_loadscr2',
				type : serverWidget.FieldType.INLINEHTML,
				label : 'Action'
			});
			
			loadScreen.defaultValue = str;
//			var loadScreen = assistant.addField("custpage_loadscr2", "inlinehtml", "");
//			loadScreen.setDefaultValue(str);
			
			option.response.writePage(objForm);
		}
		catch (e)
		{
			if (e.message != undefined)
			{
				log.error('ERROR' , e.name + ' ' + e.message);
				throw e.name + ' ' + e.message;
			}
			else
			{
				log.error('ERROR', 'Unexpected Error' , e.toString()); 
				throw error.create({
					name: '99999',
					message: e.toString()
				});
			}
		}
		finally
		{
			log.debug(stLogTitle, '>> Exit Log <<');
		}
	}
	
	function doHighAndImmediate(stHTML, objFormVals)
	{
		var stNewHTML = '';
		var stValsHTML = '';
		
		var objSLCreatorRecsResult = getSearchResults('HIGH', objFormVals);
		var ctr = 0;
        if (objSLCreatorRecsResult)
        {
        	for (var idx in objSLCreatorRecsResult)
        	{
        		var stStyle = "";
				
//				if (idx > 9)
//				{
//					stStyle = "display:none";
//				}
				
        		var stTRClassName = 'uir-list-row-tr uir-list-row-odd';
        		
        		if (ctr%2 == 0)
        		{
        			stTRClassName = 'uir-list-row-tr uir-list-row-even';
        		}
        		
        		var stURL = objURLS[objSLCreatorRecsResult[idx].getValue('name')];
        		
        		stValsHTML += '    <tr style="' + stStyle + '" class="' + stTRClassName + '" id="pos1346row' + ctr + '">';
        		stValsHTML += '        <td valign="top" class="portlettext uir-list-row-cell" style="">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_title') + '</td>';
        		stValsHTML += '        <td valign="top" class="portlettextrt uir-list-row-cell" nowrap="" style=""><a href="' + stURL + '" target="_blank">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_res_cnt') + '</a></td>';
        		stValsHTML += '        <td valign="top" class="portlettext uir-list-row-cell" style="">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_last_ref_dt') + '</td>';
        		stValsHTML += '    </tr>';
        		ctr++;
        	}
        }
        
		stNewHTML = stHTML.replace(/{{val_1}}/gi, stValsHTML);
		
		return stNewHTML;
	}
	
	function doMedium(stHTML, objFormVals)
	{
		var stNewHTML = '';
		var stValsHTML = '';
		
		var objSLCreatorRecsResult = getSearchResults('MEDIUM', objFormVals);
		
		var ctr = 0;
        if (objSLCreatorRecsResult)
        {
        	for (var idx in objSLCreatorRecsResult)
        	{
        		var stStyle = "";
				
//				if (idx > 9)
//				{
//					stStyle = "display:none";
//				}
				
        		var stTRClassName = 'uir-list-row-tr uir-list-row-odd';
        		
        		if (ctr%2 == 0)
        		{
        			stTRClassName = 'uir-list-row-tr uir-list-row-even';
        		}
        		
        		var stURL = objURLS[objSLCreatorRecsResult[idx].getValue('name')];
        		
        		stValsHTML += '    <tr style="' + stStyle + '" class="' + stTRClassName + '" id="pos1345row' + ctr + '">';
        		stValsHTML += '        <td valign="top" class="portlettext uir-list-row-cell" style="">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_title') + '</td>';
        		stValsHTML += '        <td valign="top" class="portlettextrt uir-list-row-cell" nowrap="" style=""><a href="' + stURL + '" target="_blank">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_res_cnt') + '</a></td>';
        		stValsHTML += '        <td valign="top" class="portlettext uir-list-row-cell" style="">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_last_ref_dt') + '</td>';
        		stValsHTML += '    </tr>';
        		ctr++;
        	}
        }
        
		stNewHTML = stHTML.replace(/{{val_2}}/gi, stValsHTML);
		
		return stNewHTML;
	}
	
	function doLow(stHTML, objFormVals)
	{
		var stNewHTML = '';
		var stValsHTML = '';
		
		var objSLCreatorRecsResult = getSearchResults('LOW', objFormVals);
		
		var ctr = 0;
        if (objSLCreatorRecsResult)
        {
        	for (var idx in objSLCreatorRecsResult)
        	{
        		var stStyle = "";
				
//				if (idx > 9)
//				{
//					stStyle = "display:none";
//				}
				
        		var stTRClassName = 'uir-list-row-tr uir-list-row-odd';
        		
        		if (ctr%2 == 0)
        		{
        			stTRClassName = 'uir-list-row-tr uir-list-row-even';
        		}
        		
        		var stURL = objURLS[objSLCreatorRecsResult[idx].getValue('name')];
        		
        		stValsHTML += '    <tr style="' + stStyle + '" class="' + stTRClassName + '" id="pos1344row' + ctr + '">';
        		stValsHTML += '        <td valign="top" class="portlettext uir-list-row-cell" style="">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_title') + '</td>';
        		stValsHTML += '        <td valign="top" class="portlettextrt uir-list-row-cell" nowrap="" style=""><a href="' + stURL + '" target="_blank">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_res_cnt') + '</a></td>';
        		stValsHTML += '        <td valign="top" class="portlettext uir-list-row-cell" style="">' + objSLCreatorRecsResult[idx].getValue('custrecord_suitelet_last_ref_dt') + '</td>';
        		stValsHTML += '    </tr>';
        		ctr++;
        	}
        }
        
		stNewHTML = stHTML.replace(/{{val_3}}/gi, stValsHTML);
		
		return stNewHTML;
	}
	
	function getSearchResults(stPriority, objFormVals)
	{
		var arrPriorities = [];
		switch (stPriority)
		{
			case 'HIGH':
				arrPriorities = ['3', '4'];
				break;
			case 'MEDIUM':
				arrPriorities = ['2'];
				break;
			case 'LOW':
				arrPriorities = ['1'];
				break;
		}
		
		var arrFilters = [];
		arrFilters.push({
            name: 'custrecord_suitelet_prio',
            operator: 'anyof',
            values: arrPriorities
        });

		arrFilters.push({
            name: 'custrecord_suitelet_res_cnt',
            operator: 'isnotempty'
        });

//		arrFilters.push({
//            name: 'custrecord_suitelet_emp_w_access',
//            operator: 'anyof',
//            values: [runtime.getCurrentUser()]
//        });

		var objSLCreatorRecs = search.create({
            type: 'customrecord_appwrap_suitelet_creator',
            columns: ['internalid',
                      'name',
                      'custrecord_suitelet_last_ref_dt',
                      'custrecord_suitelet_res_cnt',
                      'custrecord_suitelet_title',
                      'custrecord_suitelet_bpa',
                      'custrecord_suitelet_focus',
                      'custrecord_suitelet_rs'],
            filters: arrFilters
        }).run();

        var objSLCreatorRecsResult = objSLCreatorRecs.getRange({
            start: 0,
            end: 1000
        });
        
        return objSLCreatorRecsResult;
	}
	
	return{
		onRequest : sl_slCreatorDashboard
	};
});
