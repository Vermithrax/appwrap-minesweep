/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

/*
 * Utility Functions: M.Smith
 */

function convNull(value)
{
	if(value == null || value == undefined)
		value = '';
	return value;
}

function getAllRowsFromSearch(search, recType, searchId, filters, columns, overWriteCols)
{
	var retList = new Array();
	var srchObj = null;
	if(searchId == null || searchId == '')
		srchObj = search.create({type : recType, filters: filters, columns: columns});
	else
	{
		srchObj = search.load({id : searchId});
		var existFilters = srchObj.filters;
		var existColumns = srchObj.columns;
		
		existFilters = (existFilters == null || existFilters == '') ? new Array() : existFilters;
		existColumns = (existColumns == null || existColumns == '') ? new Array() : existColumns;
 		if(filters != null && filters != '')
		{
 			for(var idx=0; idx < filters.length; idx++)
 				existFilters.push(filters[idx]);
		}
		if(columns != null && columns != '')
		{
			if(overWriteCols == true)
				existColumns = columns;
			else
			{
				for(var idx=0; idx < columns.length; idx++)
					existColumns.push(columns[idx]);
			}
		}
		
		srchObj.filters = existFilters;
		srchObj.columns = existColumns;
	}
	
	var resultSet = srchObj.run();
	var startPos = 0, endPos = 1000;
	while (startPos <= 10000)
	{
		var options = new Object();
		options.start = startPos;
		options.end = endPos; 
		var currList = resultSet.getRange(options);
		if (currList == null || currList.length <= 0)
			break;
		if (retList == null)
			retList = currList;
		else
			retList = retList.concat(currList);
		
		if (currList.length < 1000)
			break;
		
		startPos += 1000;
		endPos += 1000;
	}
	
	return retList;
}

function getItemPricingInfo(search, currency, subsidiary)
{
	var itemFilters = new Array();
	itemFilters.push(search.createFilter({name: 'currency', join: 'pricing', operator: search.Operator.ANYOF, values: [currency]}));
	itemFilters.push(search.createFilter({name: 'subsidiary', join: null, operator: search.Operator.ANYOF, values: [subsidiary]}));
	var itemsListResults = getAllRowsFromSearch(search, 'item', 'customsearch_dsg_ecomm_item_pricing', itemFilters, null);
	var itemsList = null;
	if(itemsListResults != null && itemsListResults != '')
	{
		itemsList = new Array();
		for(var idx=0; idx < itemsListResults.length; idx++)
		{
			var itemId = itemsListResults[idx].id;
			var rate = itemsListResults[idx].getValue({name: 'unitprice', join: 'pricing'});
			itemsList['"' + itemId + '"'] = rate;
		}
		//log.debug('Checking', itemsList);
	}
	return itemsList;
}

function escapeSpecialChars(str) 
{
	if (str == undefined || str == null )
		return '';
    return str.replace(/[\"]/g, '\\"')
      .replace(/[\n]/g, '\\n')
      .replace(/[\r]/g, '\\r')
      .replace(/[\t]/g, '\\t')
    ;
}


function runSearch(search, recType, searchId, filters, columns)
{
	var srchObj = null;
	var arrSearchResults = [];
	var arrResultSet = null;
	var intSearchIndex = 0;
	
	// if search is ad-hoc (created via script)
	if(searchId == null || searchId == '')
	{
		srchObj = search.create({type : recType, filters: filters, columns: columns});
	}
	// if there is an existing saved search called and used inside the script
	else
	{
		srchObj = search.load({id : searchId});
		var existFilters = srchObj.filters;
		var existColumns = srchObj.columns;
		
		var arrNewFilters = [];
		
		for(var i = 0; i < existFilters.length; i++)
		{
	 		var stFilters = JSON.stringify(existFilters[i]);
	 		var objFilters = JSON.parse(stFilters);
	 		
	 		var objFilter = search.createFilter({
				name: objFilters.name,
				join: objFilters.join,
				operator: objFilters.operator,
				values: objFilters.values,
				formula: objFilters.formula,
				summary: objFilters.summary
			});
			
			arrNewFilters.push(objFilter);
		}
		
		arrNewFilters = (arrNewFilters == null || arrNewFilters == '') ? new Array() : arrNewFilters;
		existColumns = (existColumns == null || existColumns == '') ? new Array() : existColumns;
		
		// include additional filters created via script
 		if(filters != null && filters != '')
		{
 			for(var idx=0; idx < filters.length; idx++)
 			{
 				arrNewFilters.push(filters[idx]);
 			}
		}
 		
 		log.debug('JBC', JSON.stringify(arrNewFilters));
 		
 		// include additional columns created via script
		if(columns != null && columns != '')
		{
			for(var idx=0; idx < columns.length; idx++)
			{
				existColumns.push(columns[idx]);
			}
		}
		
		// reset original filters and columns to original ones + those passed via script
		srchObj.filters = arrNewFilters;
		srchObj.columns = existColumns;
	}
	
	var objRS = srchObj.run();
	
	// do the logic below to get all the search results because if not, you will only get 4000 max results
	do
	{
		arrResultSet = objRS.getRange(intSearchIndex, intSearchIndex + 1000);
		if (!(arrResultSet))
		{
			break;
		}

		arrSearchResults = arrSearchResults.concat(arrResultSet);
		intSearchIndex = arrSearchResults.length;
	}
	while (arrResultSet.length >= 1000);
	
	var objResults = {};
		objResults.resultSet = objRS;
		objResults.actualResults = arrSearchResults;
	
	return objResults;
}