/**
 * Copyright (c) 2017
 * AppWrap LLC
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of AppWrap LLC. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with AppWrap LLC.
 *
 * Script Name: AppWrap|Suitelet CS Creator
 *
 * Script Description:
 * Helper for suitelet creator script
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *     | Author                      | Date          | Version       | Comments                                                                |
 *     |-----------------------------|---------------|---------------|-------------------------------------------------------------------------|
 *     | M.Smith                     | July 2017     | 1.0           | Initial Version                                                         |
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *
 */

/**
 * /**
 * @NApiVersion 2.0
 * @NModuleScope SameAccount
 * @NScriptName AppWrap | CS Suitelet Creator
 * @NScriptId _appwrap_cs_suitelet_creator
 * @NScriptType ClientScript
 */

define(['N/record', 'N/error', 'N/runtime', 'N/search'], function(record, error, runtime, search)
{
	EntryPoint = {};
	
	var OBJ_FLAG = 
	{
		'_ss2' : 'ss2'
	}
	
	/*EntryPoint.pageInit = function (context)
	{
      debugger;
		jQuery(document).ready(hideRows());
	}*/
	
	//Automatically populates the mapping
	EntryPoint.fieldChanged = function (context)
	{
		var stLogTitle = 'fieldChanged';

		try
		{
			var rec = context.currentRecord;
			var stFieldName = context.fieldId;

			if(stFieldName == 'custrecord_suitelet_ss2')
			{
				//Get ss1 and ss2
				var stSS1 = rec.getValue('custrecord_suitelet_ss1');
				var stSS2 = rec.getValue('custrecord_suitelet_ss2');
				
				
				//if has ss2 
				if(stSS2)
				{
				
					//Remove all lines with ss2
					var intItemLen = rec.getLineCount('recmachcustrecord_apprwarp_suitelet_parent');
					for (var intCtr = (intItemLen-1); intCtr >= 0; intCtr--)
					{
						rec.selectLine({
							sublistId: 'recmachcustrecord_apprwarp_suitelet_parent',
							line: intCtr
						});
						var stLineSS2 = rec.getCurrentSublistValue({
							sublistId: 'recmachcustrecord_apprwarp_suitelet_parent',
							fieldId: 'custrecord_suitelet_sub_flag'
						});
							
						if(OBJ_FLAG._ss2 == stLineSS2)
						{
							
							var stId = rec.getCurrentSublistValue({
								sublistId: 'recmachcustrecord_apprwarp_suitelet_parent',
								fieldId: 'id'
							});
	
							//removeLine
							rec.removeLine({
								sublistId: 'recmachcustrecord_apprwarp_suitelet_parent',
								line: intCtr
							});

							//delete
							record.delete({
								type: 'customrecord_appwrap_suitelet_sublist',
								id: stId,
							});
						}
					}
					
					//Get Map List
					var objMapList = getSavedSearchMapping(stSS1, stSS2);
					
					for(var stLabel in objMapList)
					{
						var objMap = objMapList[stLabel];
					
						if(objMap.namemap)
						{
							//Create new lines
							rec.selectNewLine('recmachcustrecord_apprwarp_suitelet_parent');
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent','custrecord_suitelet_sub_flag',OBJ_FLAG._ss2);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent','custrecord_suitelet_sub_tag',stLabel);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent','custrecord_suitelet_sub_ss1',stSS1);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent','custrecord_suitelet_sub_ss2',stSS2);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_fn_main', objMap.name);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_fld_join_main', objMap.join);
							rec.setCurrentSublistText('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_operation', objMap.operator);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_fn_map', objMap.namemap);
							rec.setCurrentSublistValue('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_fld_join_map', objMap.joinmap);
								
							rec.commitLine('recmachcustrecord_apprwarp_suitelet_parent');
						}
					
					}
				}
			}
		}
		catch (e)
		{
			if (e.message != undefined)
			{
				log.error('ERROR' , e.name + ' ' + e.message);
				throw e.name + ' ' + e.message;
			}
			else
			{
				log.error('ERROR', 'Unexpected Error' , e.toString()); 
				throw error.create({
					name: '99999',
					message: e.toString()
				});
			}
		}

		return true;

	};

	EntryPoint.saveRecord = function (context)
	{
		var stLogTitle = 'saveRecord';
		log.debug(stLogTitle, ' -->> Enter');
		var bIsContinue = true;
		
		try
		{
			var recCurr = context.currentRecord;
			
			var stSchedule = recCurr.getValue('custrecord_suitelet_rs');
			
			if (stSchedule == '2')
			{
				var stDOW = recCurr.getValue('custrecord_suitelet_dow');
				
				if (stDOW == '' || stDOW == null)
				{
					alert('Please select the Day of the Week');
					bIsContinue = false;
				}
			}
		}
		catch (e)
		{
			if (e.message != undefined)
			{
				log.error('ERROR' , e.name + ' ' + e.message);
				throw e.name + ' ' + e.message;
			}
			else
			{
				log.error('ERROR', 'Unexpected Error' , e.toString()); 
				throw error.create({
					name: '99999',
					message: e.toString()
				});
			}
		} 
		finally 
		{
			log.debug(stLogTitle, ' <<-- Exit' );
		}
		
		return bIsContinue;
		
	};
	
	function getSavedSearchMapping(stSS1, stSS2)
	{
		var stLogTitle = 'getSavedSearchMapping';
		
		
		var objNewMap = {};
		
		var objMapMain = getMapping(stSS1);
		var objMapFilter1 = getMapping(stSS2);

		for(var intCtr in objMapMain)
		{
			var objMap = objMapMain[intCtr];
			var objCol = objMap.objColumn;
			
			if(objCol)
			{
				var stLabel = objCol.label;
			
				if(!objNewMap[stLabel])
				{
					objNewMap[stLabel] = {};
					objNewMap[stLabel].name = convNull(objCol.name);
					objNewMap[stLabel].join = convNull(objCol.join);
					objNewMap[stLabel].type = convNull(objCol.type);
					
					//Default operator
					objNewMap[stLabel].operator = setDefaultOperator(objNewMap[stLabel].type);
				}
			}
		}
		
		for(var intCtr in objMapFilter1)
		{
			var objMap = objMapFilter1[intCtr];
			var objCol = objMap.objColumn;
			
			if(objCol)
			{
				var stLabel = objCol.label;
			
				if(objNewMap[stLabel])
				{
					objNewMap[stLabel].namemap = convNull(objCol.name);
					objNewMap[stLabel].joinmap = convNull(objCol.join);
				}
			}
		}
		
		log.debug(stLogTitle, 'objNewMap ='+JSON.stringify(objNewMap));

		return objNewMap;
	}
	
	function convNull(value)
	{
		if(value == null || value == undefined)
			value = '';
		return value;
	}
	
	function  setDefaultOperator(stFldType)
	{
		var stDefaultOperator = '';
		switch(stFldType) {
			case 'select':
				stDefaultOperator = 'anyof'
				break;
			case 'date':
				stDefaultOperator = 'on'
				break;
			case 'datetime':
				stDefaultOperator = 'on'
				break;
			default:
				stDefaultOperator = 'is'
		}
		return stDefaultOperator;
	}


	function getMapping(stSavedSearch)
	{
		var stLogTitle = 'getMapping';
		
		log.debug(stLogTitle, 'FOR  stSavedSearch ='+stSavedSearch);

		//Load the filter saved search 2
		var arrFilter2 = [];
		var arrResult2 = runSearch(search, null, stSavedSearch, arrFilter2);
		var arrColumns2 = arrResult2.columns;
		var objMap = {};
		
		//Get the List Filter from the saved search and add it to the form
		for(var intColCtr=0; intColCtr < arrColumns2.length; intColCtr++)
		{
			var objColumn = arrColumns2[intColCtr];
			var stColumn = JSON.stringify(objColumn);
			var objColumnHold = JSON.parse(stColumn);
			
			//Getters
			var stLabel = convNull(objColumnHold.label);
			var stType = convNull(objColumnHold.type);

			//If starts with M and a number, store on an object
			if (stLabel.match(/^M+[0-9]+/))
			{
				objMap[intColCtr] = {};
				objMap[intColCtr].objColumn = {};
				objMap[intColCtr].objColumn = objColumn;
				objMap[intColCtr].stLabel = stLabel;
			}
		}
		//Generate the saved search result on the list
		//Get only the first result!!!
		var intLineCount = 0;
		arrResult2.each(function(objResult) 
		{

			//For each column
			for(var intColCtr=0; intColCtr < arrColumns2.length; intColCtr++)
			{
				//If not included, skip it
				if(!objMap[intColCtr]) continue;
	
				var objColumn = objMap[intColCtr].objColumn;
				
				//Get Value
				var stValue = objResult.getText(objColumn);
				if(!stValue)
				{
					stValue = objResult.getValue(objColumn);
				}
				
				//Set Value
				if(stValue)
				{
					objMap[intColCtr].stValue = stValue;
				}

			}
			
			return false; //will give only 1 result
		});
		
		log.debug(stLogTitle, 'objMap ='+JSON.stringify(objMap));

		return objMap;
	}
	
	function runSearch(search, recType, searchId, filters, columns)
	{
		var srchObj = null;
		if(searchId == null || searchId == '')
			srchObj = search.create({type : recType, filters: filters, columns: columns});
		else
		{
			srchObj = search.load({id : searchId});
			var existFilters = srchObj.filters;
			var existColumns = srchObj.columns;
			
			existFilters = (existFilters == null || existFilters == '') ? new Array() : existFilters;
			existColumns = (existColumns == null || existColumns == '') ? new Array() : existColumns;
			if(filters != null && filters != '')
			{
				for(var idx=0; idx < filters.length; idx++)
					existFilters.push(filters[idx]);
			}
			if(columns != null && columns != '')
			{
				for(var idx=0; idx < columns.length; idx++)
					existColumns.push(columns[idx]);
			}
			
			srchObj.filters = existFilters;
			srchObj.columns = existColumns;
		}
		
		var resultSet = srchObj.run();
		
		return resultSet;
	}
	
	return EntryPoint;
});


