/**
 * Copyright (c) 2017
 * AppWrap LLC
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of AppWrap LLC. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with AppWrap LLC.
 *
 * Script Name: AppWrap|Suitelet SL Creator
 *
 * Script Description:
 * Automatically creates a suitelet based on the 'Appwrap Suitelet Creator' custom record.
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *     | Author                      | Date          | Version       | Comments                                                                |
 *     |-----------------------------|---------------|---------------|-------------------------------------------------------------------------|
 *     | M.Smith                     | July 2017     | 1.0           | Initial Version                                                         |
 *     | J.Cuanan                    | Sept 2017     | 1.1           | Customization - Exclusion                                               |
 *     | J.Cuanan                    | Oct 2017      | 1.2           | Customization - Scheduling                                              |
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *
 */

/**
 * @NApiVersion 2.0
 * @NModuleScope SameAccount
 * @NScriptName Appwrap | SL Suitelet Creator
 * @NScriptId _sl_suitelet_creator
 * @NScriptType suitelet
 */
define(['N/email', 'N/format', 'N/record', 'N/search', 'N/ui/serverWidget', 'N/runtime',  'N/xml', 'N/error', 'N/file', 'N/render', './appwrap_UtilityFunctions.js'], 

function(email, format, record, search, serverWidget, runtime, xml, error, file, render)
{
	var OBJ_TAGS = 
	{
		'_do_not_show_result' : 'NA',
		'_filter_search' : 'F',
		'_map_search' : 'M'
	};
	
	var HTML_SCRIPT_FLAGFUNCTION = ''
        + '<script>'
        + 'function doPrint(format)'
        + '{'
        + '    if (format == \'pdf\')'
        + '    {'
        + '        nlapiSetFieldValue(\'custpage_action\', \'PRINT_PDF\');'
        + '    }'
        + '    else if (format == \'csv\')'
        + '    {'
        + '        nlapiSetFieldValue(\'custpage_action\', \'PRINT_CSV\');'
        + '    }'
        + ''
        + '    window.ischanged = false;'
        + '    document.forms[0].submit();'
        + '}'
        + 'function doFilter()'
        + '{'
        + '    nlapiSetFieldValue(\'custpage_action\', \'APPLY_FILTER\');'
        + '    window.ischanged = false;'
        + '    document.forms[0].submit();'
        + '}'
        + '</script>';
	
	function suitelet_suiteletCreator(option)
	{
		var stLogTitle = 'suitelet_suiteletCreator';
		
//		try
//		{
			log.debug(stLogTitle, '>> Entry Log <<');
	
			// Get the Custom RecordId from the script parameter
			var stParamSuiteletRecId = runtime.getCurrentScript().getParameter('custscript_suitelet_rec_id');
			
			// Throw an error if there is this is blank
			if(!stParamSuiteletRecId)
			{
				throw error.create({
					name: 'MISSING_REQ_ARG',
					message: 'Script parameters should not be empty'
				});
			}

			var stAction = option.request.parameters.custpage_action;
			log.debug(stLogTitle, 'stAction = ' + stAction);
			
			switch (stAction)
			{
				case 'PRINT_PDF':
					exportToPDF(option, stParamSuiteletRecId);
					break;
				case 'PRINT_CSV':
					var csvFile = exportToCSV(option, stParamSuiteletRecId);
					option.response.writeFile(csvFile);
					break;
				case 'APPLY_FILTER':
//					var objForm = showSuiteletPage(option, stParamSuiteletRecId);
//					option.response.writePage(objForm);
//					break;
				default:
					// Show the suitelet form
					var objForm = showSuiteletPage(option, stParamSuiteletRecId);
					var stRunType = option.request.parameters.custpage_run_type;
					log.debug(stLogTitle, 'stRunType = ' + stRunType);
					
					if (stRunType != 'scheduled')
					{
						option.response.writePage(objForm);
					}
					
					break;
			}
//		}
//		catch (e)
//		{
//			if (e.message != undefined)
//			{
//				log.error('ERROR' , e.name + ' ' + e.message);
//				throw e.name + ' ' + e.message;
//			}
//			else
//			{
//				log.error('ERROR', 'Unexpected Error' , e.toString()); 
//				throw error.create({
//					name: '99999',
//					message: e.toString()
//				});
//			}
//		}
//		finally
//		{
//			log.debug(stLogTitle, '>> Exit Log <<');
//		}

	}
	
	/**
	 * Show the suitelet form
	 * @option
	 * @stParamCourseSearch
	 */
	function showSuiteletPage(option, stParamSuiteletRecId)
	{
		var stLogTitle = 'showSuiteletPage';
		log.debug(stLogTitle, 'Creating the form...');
		
		// Get the Suitelet parameters that will be loaded based on the id on the script parameter
		var recSuiteletParams = record.load({
			type: 'customrecord_appwrap_suitelet_creator', 
			id: stParamSuiteletRecId, 
			isDynamic: true 
		});
		
		// Get the data
		var objParam = 
		{
			'_suitelet_title' : recSuiteletParams.getValue('custrecord_suitelet_title'),
			'_suitelet_headermsg': recSuiteletParams.getValue('custrecord_suitelet_headermsg'),
			'_suitelet_sublistmsg': recSuiteletParams.getValue('custrecord_suitelet_sublistmsg'),
			'_suitelet_ss1' : recSuiteletParams.getValue('custrecord_suitelet_ss1'),
			'_suitelet_ss2' : recSuiteletParams.getValue('custrecord_suitelet_ss2'),
			'_suitelet_listhdr': recSuiteletParams.getValue('custrecord_suitelet_listhdr'),
			'_suitelet_pdf': recSuiteletParams.getValue('custrecord_suitelet_pdf'),
			'_suitelet_csv': recSuiteletParams.getValue('custrecord_suitelet_csv'),
			'_suitelet_pdf_layout_id': recSuiteletParams.getValue('custrecord_pdf_layout_id'),
		};
		
		log.debug(stLogTitle, 'objParam ='+JSON.stringify(objParam));
		
		// Create Form
		var objForm = serverWidget.createForm({
			title: objParam._suitelet_title,
			hideNavBar : false
		});
		
		objForm.clientScriptFileId = '../Suitelet Creator/AppWrap_CS_SuiteletCreatorValidator.js';
		
		// event handler on to what page/action will be shown/executed
		var objFldAction = objForm.addField({
			id : 'custpage_action',
			type : serverWidget.FieldType.TEXT,
			label : 'Action'
		});
		
		objFldAction.defaultValue = 'APPLY_FILTER';
		
		objFldAction.updateDisplayType(
	    {
	        displayType: 'HIDDEN'
	    });
		
		// html code to include button functionalities
		var objFldHTML = objForm.addField({
			id : 'custpage_html',
			type : serverWidget.FieldType.INLINEHTML,
			label : 'HTML'
		});
		
		objFldHTML.defaultValue = HTML_SCRIPT_FLAGFUNCTION;
		
		// Add Header Message if has value
		if(objParam._suitelet_headermsg)
		{
//			var objFldMessage = objForm.addField({
//				id : 'custpage_message',
//				type : serverWidget.FieldType.INLINEHTML,
//				label : 'Header Message'
//			});
//				
//			var arrHTML = new Array();
//	        arrHTML.push('<html><body>');
//	        arrHTML.push('<div style="margin-top:10px"><font style="font-size: initial;">'+objParam._suitelet_headermsg+'</font> <br/> </div>'); 
//	        arrHTML.push('</body></html><BR>');
			var objFldMessage = objForm.addField({
				id : 'custpage_description',
				type : serverWidget.FieldType.LABEL,
				label : objParam._suitelet_headermsg
			});
//	        objFldMessage.defaultValue = arrHTML.join('');
//	        objFldMessage.layoutType = serverWidget.FieldLayoutType.OUTSIDEABOVE;
//	        objFldMessage.breakType = serverWidget.FieldBreakType.STARTCOL;
		}
		
		var tab = objForm.addSubtab({
			id : 'custpage_tabid',
			label : objParam._suitelet_listhdr
		});
		
		tab.helpText = objParam._suitelet_sublistmsg;
		
		// Add the sublist
		var objSublist = objForm.addSublist({
			id : 'custpage_sublist',
			type : serverWidget.SublistType.LIST,
			label : objParam._suitelet_listhdr,
			tab: 'custpage_tabid'
		});
		
		// checkbox to be used in excluding/filtering search results
		objSublist.addField({
			id : 'custpage_chkbx',
			type : serverWidget.FieldType.CHECKBOX,
			label : 'Exclude'
		});
		
		var arrResult = getResult(recSuiteletParams);
		
		var arrColumns = arrResult.resultSet.columns;
		var objColumns = {};
		
		log.debug(stLogTitle, 'arrResult ='+JSON.stringify(arrResult));
		
		// Suitelet Creator record id
		var objFldSLCreatorId = objForm.addField({
			id : 'custpage_sl_id',
			type : serverWidget.FieldType.TEXT,
			label : 'Suitelet Creator ID'
		});
		
		objFldSLCreatorId.updateDisplayType({
            displayType: 'HIDDEN'
        });
		
		objFldSLCreatorId.defaultValue = stParamSuiteletRecId;
		
		// exluded field
		var objFldExclude = objForm.addField({
			id : 'custpage_exclude',
			type : serverWidget.FieldType.SELECT,
			label : 'Exclude Column Label'
		});
		
		objFldExclude.addSelectOption({
			value : '1',
			text : ' ',
			isSelected : true
		});
		
		objForm.addField({
			id : 'custpage_spacer1',
			type : serverWidget.FieldType.LABEL,
			label : ' '
		});
		
		// Get the List Header from the saved search and add it to the form
		for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
		{
			var objColumn = arrColumns[intColCtr];
			var stColumn = JSON.stringify(objColumn);

//			log.debug(stLogTitle, 'XXXXXXXXXXX stColumn = ' + stColumn);
			
			var objColumnHold = JSON.parse(stColumn);
			
			// Getters
			var stLabel = convNull(objColumnHold.label);
			var stType = convNull(objColumnHold.type);
			var stName = convNull(objColumnHold.name);
			
			// Do not include labels with 'NA' tags
			if (stLabel == OBJ_TAGS._do_not_show_result || stLabel.match(/^M+[0-9]+/))
			{
				continue;
			}

			var stFieldType = serverWidget.FieldType.TEXTAREA;
			if(stType == 'url')
			{
				stFieldType = serverWidget.FieldType.URL;
			}
			
			objSublist.addField({
				id : 'custpage_' + intColCtr,
				type : stFieldType,
				label : stLabel
			});
			
			// add Exclude Column Label select options
			objFldExclude.addSelectOption({
				value : stName + '_' + intColCtr,// internal id of the column field
				text : stLabel,// label of the column field
				isSelected : false,
			});
			
			// Save objColumns
			objColumns[intColCtr] = {};
			objColumns[intColCtr] = objColumn;
		}	
		log.debug(stLogTitle, 'objColumns ='+JSON.stringify(objColumns));
		
		// OK -----------------------------------------------------
		
		// Generate the saved search result on the list
		var intLineCount = 0;
		
//		arrResult.actualResults.each(function(objResult) 
//		{
//			//For each column
//			for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
//			{
//				var objColumn = objColumns[intColCtr];
//
//				//If not included in the header, skip it
//				if(!objColumn) continue;
//
//				//Get Value
//				var stValue = objResult.getText(objColumn);
//				if(!stValue)
//				{
//					stValue = objResult.getValue(objColumn);
//				}
//				
//				//Set Value
//				if(stValue)
//				{
//					objSublist.setSublistValue(
//					{
//		            	 id : 'custpage_' + intColCtr,
//		            	 line : intLineCount,
//		            	 value : stValue
//					}); 
//				}
//			}
//			
//			intLineCount++;
//			return true;
//		});
		
		for (var idxx in arrResult.actualResults)
        {
			//For each column
			for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
			{
				var objColumn = objColumns[intColCtr];

				//If not included in the header, skip it
				if(!objColumn) continue;

				//Get Value
				var stValue = arrResult.actualResults[idxx].getText(objColumn);
				if(!stValue)
				{
					stValue = arrResult.actualResults[idxx].getValue(objColumn);
				}
				
				//Set Value
				if(stValue)
				{
					objSublist.setSublistValue(
					{
		            	 id : 'custpage_' + intColCtr,
		            	 line : intLineCount,
		            	 value : stValue
					}); 
				}
			}
			
			intLineCount++;
        }
		
		objForm.addSubmitButton({
    		label : 'Apply Filter'
    	});
		
		if (objParam._suitelet_pdf == true)
		{
			objForm.addButton({
	    		id : 'custpage_pdf',
	    		label : 'Print as PDF',
	    		functionName : 'doPrint(\'pdf\')'
	    	});
		}
		
		if (objParam._suitelet_csv == true)
		{
			objForm.addButton({
	    		id : 'custpage_csv',
	    		label : 'Print as CSV',
	    		functionName : 'doPrint(\'csv\')'
	    	});
		}
		
		// create Run Details record
		var objRunDetailsRec = record.create({
            type: 'customrecord_search_run_details'
        });
		
		objRunDetailsRec.setValue({
            fieldId: 'custrecord_srd_srch',
            value: stParamSuiteletRecId
        });
		
		objRunDetailsRec.setValue({
            fieldId: 'custrecord_srd_is_latest',
            value: true
        });
		
		objRunDetailsRec.setValue({
            fieldId: 'custrecord_srd_srch_rslts_cnt',
            value: intLineCount
        });
		
		var stDate = format.format({
			value: new Date(),
			type: format.Type.DATE
		});
		
		stDate = format.format({
			value: stDate,
			type: format.Type.DATE
		});
		
		stDate = format.parse({
			value: stDate,
			type: format.Type.DATE
		});
		
		var stDateTime = format.format({
			value: new Date(),
			type: format.Type.DATETIME
		});
		
		stDateTime = format.format({
			value: stDateTime,
			type: format.Type.DATETIME
		});
		
		stDateTime = format.parse({
			value: stDateTime,
			type: format.Type.DATETIME
		});
        
		var stSendEmail = option.request.parameters.custpage_send_email;
		log.debug(stLogTitle, 'stSendEmail = ' + stSendEmail);
		
		if (stSendEmail == 'send_email')
		{
//			var objCsvFile = exportToCSV(option, stParamSuiteletRecId);
			// compose email and attach csv file
			var arrFiles = [];
			// override "extend" property; use file created
//			arrFiles['extend'] = objCsvFile;
			
			// send email
			email.send({
			    author: -5,
			    recipients: 1692,
			    subject: 'Sample Email Subject',
			    body: 'Sample Email Body',
			    attachments: arrFiles
			});
		}
		
		var stRunType = option.request.parameters.custpage_run_type;
		log.debug(stLogTitle, 'stRunType = ' + stRunType);
		
		if (stRunType == 'scheduled')
		{
			objRunDetailsRec.setValue({
	            fieldId: 'custrecord_srd_last_sc_run_ts',
	            value: stDate
	        });
		}
		else
		{
			objRunDetailsRec.setValue({
	            fieldId: 'custrecord_srd_last_run_ts',
	            value: stDateTime
	        });
		}
		
		var stRunDetailsRecId = objRunDetailsRec.save({
            enableSourcing: true,
            ignoreMandatoryFields: true
        });
		
		log.audit(stLogTitle, 'Successfully created Run Details record with internal id = ' + stRunDetailsRecId);
		
		recSuiteletParams.setValue({
			fieldId: 'custrecord_suitelet_res_cnt',
            value: intLineCount
		});
		
		recSuiteletParams.setValue({
			fieldId: 'custrecord_suitelet_last_ref_dt',
            value: stDateTime
		});
		
		var stSLCreatorRecId = recSuiteletParams.save({
            enableSourcing: true,
            ignoreMandatoryFields: true
        });
		
		log.audit(stLogTitle, 'Successfully updated Suitelet Creator record with internal id = ' + stSLCreatorRecId);
		
		return objForm;
	}
	
	function exportToPDF(option, stParamSuiteletRecId)
	{
		var stLogTitle = 'exportToPDF';
		var headerContents = '';
		var bodyContents = '';
		var pdfContents = '';

		// Get the Suitelet parameters that will be loaded based on the id on the script parameter
		var recSuiteletParams = record.load({
			type: 'customrecord_appwrap_suitelet_creator', 
			id: stParamSuiteletRecId, 
			isDynamic: true 
		});
	
		var stTitle = recSuiteletParams.getValue('custrecord_suitelet_title').replace(/\s+/g, '');;
		var stLayoutID = recSuiteletParams.getValue('custrecord_suitelet_pdf_layout_id');

		pdfContents = file.load({
			id : stLayoutID
		}).getContents();
		
		var arrResult = getResult(recSuiteletParams)
		var arrColumns = arrResult.columns;
		var objColumns = {};
		
		log.debug(stLogTitle, 'arrResult ='+JSON.stringify(arrResult));
		
		// create some arrays that will be populated from the saved search results
        var content = [];
        var temp = [];
        var x = 0;
		
		// Get the List Header from the saved search and use it as CSV column header
		for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
		{
			var objColumn = arrColumns[intColCtr];
			var stColumn = JSON.stringify(objColumn);
			var objColumnHold = JSON.parse(stColumn);
			
			// Getters
			var stLabel = convNull(objColumnHold.label);
			
			// Do not include labels with 'NA' tags
			if (stLabel == OBJ_TAGS._do_not_show_result || stLabel.match(/^M+[0-9]+/))
			{
				continue;
			}

			temp.push('<td>' + stLabel + '</td>');
			
			// Save objColumns
			objColumns[intColCtr] = {};
			objColumns[intColCtr] = objColumn;
		}
		
		log.debug(stLogTitle, 'objColumns ='+JSON.stringify(objColumns));
		
		content[x] = temp.join('');
        x++;
        temp = [];
		
		// Generate the saved search result as values
		arrResult.each(function(objResult) 
		{
			// For each column
			for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
			{
				
				var objColumn = objColumns[intColCtr];
				
				// If not included in the header, skip it
				if(!objColumn) continue;
				
				// Get Value
				var stValue = objResult.getText(objColumn);
				if(!stValue)
				{
					stValue = objResult.getValue(objColumn);
				}
				
				temp.push('<td>' + xml.escape(stValue) + '</td>');
			}
			
			// Taking the content of the temp array and assigning it to the Content Array.
            content[x] = temp.join('');
            x++;
            temp = [];
            return true;
		});

        // Looping through the content array and assigning it to the contents string variable.
		headerContents += '<tr>' + content[0].toString() + '</tr>';
        for (var z = 1; z < content.length; z++)
        {
        	bodyContents += '<tr>' + content[z].toString() + '</tr>';
        }
        
        pdfContents = pdfContents.replace('{{header}}', headerContents).replace('{{body}}', bodyContents);

        log.debug(stLogTitle, 'pdfContents = ' + pdfContents);
        
		// Create Form
		var objForm = serverWidget.createForm({
			title: 'test',
			hideNavBar : false
		});
        
		
        var objFldHTML = objForm.addField({
			id : 'custpage_longtext',
			type : serverWidget.FieldType.LONGTEXT,
			label : 'LONGTEXT'
		});
		
		objFldHTML.defaultValue = pdfContents;
		
		option.response.writePage(objForm);
		

		/*
		// create file
        var pdfFile = render.xmlToPdf({
        	name: 'TEST_PDF.pdf',
		    xmlString: pdfContents
		});
       
		option.response.writeFile(pdfFile);
		*/
	
	}
	
	function exportToCSV(option, stParamSuiteletRecId)
	{
		var stLogTitle = 'exportToCSV';
		var contents = '';

		// Get the Suitelet parameters that will be loaded based on the id on the script parameter
		var recSuiteletParams = record.load({
			type: 'customrecord_appwrap_suitelet_creator', 
			id: stParamSuiteletRecId, 
			isDynamic: true 
		});
		var stTitle = recSuiteletParams.getValue('custrecord_suitelet_title').replace(/\s+/g, '');;
		
		
		var arrResult = getResult(recSuiteletParams);
		var arrColumns = arrResult.columns;
		var objColumns = {};
		
		// create some arrays that will be populated from the saved search results
        var content = [];
        var temp = [];
        var x = 0;
		
		// Get the List Header from the saved search and use it as CSV column header
		for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
		{
			var objColumn = arrColumns[intColCtr];
			var stColumn = JSON.stringify(objColumn);
			var objColumnHold = JSON.parse(stColumn);
			
			// Getters
			var stLabel = convNull(objColumnHold.label);
			
			// Do not include labels with 'NA' tags
			if (stLabel == OBJ_TAGS._do_not_show_result || stLabel.match(/^M+[0-9]+/))
			{
				continue;
			}

			temp.push(stLabel);
			
			// Save objColumns
			objColumns[intColCtr] = {};
			objColumns[intColCtr] = objColumn;
		}
		
		log.debug(stLogTitle, 'objColumns ='+JSON.stringify(objColumns));
		
		content[x] = temp;
        x++;
        temp = [];
		
		// Generate the saved search result as values
		arrResult.each(function(objResult) 
		{
			// For each column
			for(var intColCtr=0; intColCtr < arrColumns.length; intColCtr++)
			{
				
				var objColumn = objColumns[intColCtr];
				
				// If not included in the header, skip it
				if(!objColumn) continue;
				
				// Get Value
				var stValue = objResult.getText(objColumn);
				if(!stValue)
				{
					stValue = objResult.getValue(objColumn);
				}
				
				temp.push(stValue);
			}
			
			// Taking the content of the temp array and assigning it to the Content Array.
            content[x] = temp;
            x++;
            temp = [];
            return true;
		});

        // Looping through the content array and assigning it to the contents string variable.
        for (var z = 0; z < content.length; z++)
        {
            contents += content[z].toString() + '\n';
        }

        // create file
		var csvFile = file.create({
		    name: stTitle+'.csv',
		    fileType: file.Type.CSV,
		    contents: contents,
		});
      
        return csvFile;
	}
	
	// remove column number in the end of the filter name (column number came from the option value during suitelet load of Exclude Column Label field)
	function getFilterName(stName)
	{
		var arrName = stName.split('_');
		
		var stActualName = '';
		
		for (var i = 0; i < arrName.length-1; i++)
		{
			stActualName += arrName[i];
		}
		
		return stActualName;
	}

	function getResult(recSuiteletParams)
	{
		var stLogTitle = 'getResult';
		
		// Get the data
		var objParam = 
		{
			'_suitelet_ss1' : recSuiteletParams.getValue('custrecord_suitelet_ss1'),
			'_suitelet_ss2' : recSuiteletParams.getValue('custrecord_suitelet_ss2'),
			'_suitelet_pdf': recSuiteletParams.getValue('custrecord_suitelet_pdf'),
			'_suitelet_csv': recSuiteletParams.getValue('custrecord_suitelet_csv'),
		};
		
		log.debug(stLogTitle, 'objParam ='+JSON.stringify(objParam));
		
		var arrFilter = [];
		
		// include excluded fields in the filters
		var intExLen = recSuiteletParams.getLineCount('recmachcustrecord_ex_sl_parent');
		
		for (var intCtr = 0; intCtr < intExLen; intCtr++)
		{
			var stName = getFilterName(recSuiteletParams.getSublistValue('recmachcustrecord_ex_sl_parent','custrecord_ex_field_id', intCtr));
			var stValue = recSuiteletParams.getSublistValue('recmachcustrecord_ex_sl_parent','custrecord_ex_value', intCtr);
			var flValue = (stValue/100)*100;
			
			if (!isNaN(flValue) && stName.indexOf('date') < 0)
			{
				arrFilter.push(search.createFilter({name: 'formulanumeric', operator: 'notequalto', values: stValue, formula: '{' + stName + '}'}));
			}
			else
			{
				arrFilter.push(search.createFilter({name: 'formulatext', operator: 'isnot', values: stValue, formula: '{' + stName + '}'}));
			}
			
		}
		
		if(objParam._suitelet_ss2)
		{
			var intItemLen = recSuiteletParams.getLineCount('recmachcustrecord_apprwarp_suitelet_parent');
			var objMapping = getMapping(objParam._suitelet_ss2);
			for (var intCtr = (intItemLen-1); intCtr >= 0; intCtr--)
			{
				
				var stLabel = recSuiteletParams.getSublistValue('recmachcustrecord_apprwarp_suitelet_parent','custrecord_suitelet_sub_tag', intCtr);
				if(objMapping[stLabel])
				{
					var stName = recSuiteletParams.getSublistValue('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_fn_main', intCtr);
					var stJoin =  recSuiteletParams.getSublistValue('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_fld_join_main', intCtr);
					var stOperator =  recSuiteletParams.getSublistText('recmachcustrecord_apprwarp_suitelet_parent', 'custrecord_suitelet_sub_operation', intCtr);
					var stValue = objMapping[stLabel].stValue;
					
					if(stJoin)
					{
						arrFilter.push(search.createFilter({name: stName, join : stJoin, operator: stOperator, values: stValue})) ;
					}
					else 
					{
						arrFilter.push(search.createFilter({name: stName, operator: stOperator, values: stValue})) ;
					}
				}
			}
		}
		
		// Load the main saved search
		var arrResult = runSearch(search, null, objParam._suitelet_ss1, arrFilter);
		
		log.debug(stLogTitle, 'arrResult ='+JSON.stringify(arrResult));
		
		return arrResult;
	}
	
	function getMapping(stSavedSearch)
	{
		var stLogTitle = 'getMapping';
		
		log.debug(stLogTitle, 'FOR  stSavedSearch ='+stSavedSearch);

		// Load the filter saved search 2
		var arrFilter2 = [];
		var arrResult2 = runSearch(search, null, stSavedSearch, arrFilter2);
		var arrColumns2 = arrResult2.resultSet.columns;
		var objMap = {};
		var objFinalMap = {};
		
		// Get the List Filter from the saved search and add it to the form
		for(var intColCtr=0; intColCtr < arrColumns2.length; intColCtr++)
		{
			var objColumn = arrColumns2[intColCtr];
			var stColumn = JSON.stringify(objColumn);
			var objColumnHold = JSON.parse(stColumn);
			
			// Getters
			var stLabel = convNull(objColumnHold.label);
			var stType = convNull(objColumnHold.type);

			// If starts with M and a number, store on an object
			if (stLabel.match(/^M+[0-9]+/))
			{
				objMap[intColCtr] = {};
				objMap[intColCtr].objColumn = {};
				objMap[intColCtr].objColumn = objColumn;
				objMap[intColCtr].stLabel = stLabel;
			}
		}
		// Generate the saved search result on the list
		// Get only the first result!!!
		var intLineCount = 0;
		for (var idxx in arrResult2.actualResults)
        {
			if (intLineCount == 0)
			{
				// For each column
				for(var intColCtr=0; intColCtr < arrColumns2.length; intColCtr++)
				{
					// If not included, skip it
					if(!objMap[intColCtr]) continue;
		
					var objColumn = objMap[intColCtr].objColumn;
					
					// Get Value
					var stValue = arrResult2.actualResults[idxx].getText(objColumn);
					if(!stValue)
					{
						stValue = arrResult2.actualResults[idxx].getValue(objColumn);
					}
					
					// Set Value
					if(stValue)
					{
						objFinalMap[objMap[intColCtr].stLabel] = {};
						objFinalMap[objMap[intColCtr].stLabel].stValue = stValue;
					}

				}
			}
			else
			{
				break;
			}
			
			intLineCount++;
//			return false; // will give only 1 result
		};
		
		log.debug(stLogTitle, 'objFinalMap ='+JSON.stringify(objFinalMap));

		return objFinalMap;
	}
	
	return{
		onRequest : suitelet_suiteletCreator
	};
});
