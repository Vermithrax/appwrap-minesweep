/**
 * Copyright (c) 2017
 * AppWrap LLC
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of AppWrap LLC. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with AppWrap LLC.
 *
 * Script Name: AppWrap|Suitelet CS Creator
 *
 * Script Description:
 * Helper for suitelet creator script
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *     | Author                      | Date          | Version       | Comments                                                                |
 *     |-----------------------------|---------------|---------------|-------------------------------------------------------------------------|
 *     | J.Cuanan                    | Sep  2017     | 1.0           | Initial Version                                                         |
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *
 */

/**
 * /**
 * @NApiVersion 2.0
 * @NModuleScope SameAccount
 * @NScriptName AppWrap | CS Suitelet Creator
 * @NScriptId _appwrap_cs_suitelet_creator
 * @NScriptType ClientScript
 */

define(['N/record', 'N/error'], function(record, error)
{
	EntryPoint = {};
	
	//Automatically populates the mapping
	EntryPoint.fieldChanged = function (context)
	{
		var stLogTitle = 'fieldChanged';

		try
		{}
		catch (e)
		{
			if (e.message != undefined)
			{
				log.error('ERROR' , e.name + ' ' + e.message);
				throw e.name + ' ' + e.message;
			}
			else
			{
				log.error('ERROR', 'Unexpected Error' , e.toString()); 
				throw error.create({
					name: '99999',
					message: e.toString()
				});
			}
		}

		return true;

	};

	EntryPoint.saveRecord = function (context)
	{
		debugger;
		var stLogTitle = 'saveRecord';
		log.debug(stLogTitle, ' -->> Enter' );
	
		try
		{
			var recCurr = context.currentRecord;
			var stSLCreatorId = recCurr.getValue({
			    fieldId : 'custpage_sl_id'
			});
			
			// get line count of the suitelet sublist
			var intLineCount = recCurr.getLineCount({
			    sublistId : 'custpage_sublist'
			});
			
			// get Exclude Column Label value
			var stColumn = recCurr.getText({
			    fieldId : 'custpage_exclude'
			});
			
			// get Exclude Column Label name
			var stColumnInternalId = recCurr.getValue({
			    fieldId : 'custpage_exclude'
			});
			
			var arrColumnInternalId = stColumnInternalId.split('_');
			var stColNum = arrColumnInternalId[arrColumnInternalId.length-1];
			
			var recSL = record.load({
				type : 'customrecord_appwrap_suitelet_creator', 
				id : stSLCreatorId, 
				isDynamic : true 
			});
			
			var arrVals = [];
			var arrExcludeColVal = [];
			var bIsExistingEx = false;
			var bIsCreatedEx = false;
			
			var intExLen = recSL.getLineCount('recmachcustrecord_ex_sl_parent');
			
			for (var intCtr = 0; intCtr < intExLen; intCtr++)
			{
				var stName = recSL.getSublistValue('recmachcustrecord_ex_sl_parent', 'custrecord_ex_field', intCtr);
				var stNameId = recSL.getSublistValue('recmachcustrecord_ex_sl_parent', 'custrecord_ex_field_id', intCtr);
				var stValue = recSL.getSublistValue('recmachcustrecord_ex_sl_parent', 'custrecord_ex_value', intCtr);
				
				arrVals.push(stSLCreatorId + '___' + stName + '___' + stNameId + '___' + stValue);
			}

			for (var i = 0; i < intLineCount; i++)
			{
				var stExcludeCb = recCurr.getSublistValue({
					sublistId : 'custpage_sublist',
					fieldId : 'custpage_chkbx',
					line : i
				});
				
				// continue if Exclude checkbox is checked
				if (stExcludeCb == true)
				{
					var stExcludeColVal = recCurr.getSublistValue({
						sublistId : 'custpage_sublist',
						fieldId : 'custpage_' + stColNum,
						line : i
					});
					
					var stCurrVal = stSLCreatorId + '___' + stColumn + '___' + stColumnInternalId + '___' + stExcludeColVal;
					
					// check if Exception is not yet existing
					for (var m = 0; m < arrVals.length; m++)
					{
						if (stCurrVal == arrVals[m])
						{
							bIsExistingEx = true;
						}
					}
					
					// check if Exception is not yet created
					for (var m = 0; m < arrExcludeColVal.length; m++)
					{
						if (stExcludeColVal == arrExcludeColVal[m])
						{
							bIsCreatedEx = true;
						}
					}
					
					if (!bIsCreatedEx && !bIsExistingEx)
					{
						var objExRec = record.create({
	                        type: 'customrecord_appwrap_suitelet_creator_ex'
	                    });
						
						objExRec.setValue({
	                        fieldId: 'custrecord_ex_sl_parent',
	                        value: stSLCreatorId
	                    });
						
						objExRec.setValue({
	                        fieldId: 'custrecord_ex_field',
	                        value: stColumn
	                    });
						
						objExRec.setValue({
	                        fieldId: 'custrecord_ex_field_id',
	                        value: stColumnInternalId
	                    });
						
						objExRec.setValue({
	                        fieldId: 'custrecord_ex_value',
	                        value: stExcludeColVal
	                    });
	                    
						var stExRec = objExRec.save({
	                        enableSourcing: true,
	                        ignoreMandatoryFields: true
	                    });
					}
					
					arrExcludeColVal.push(stExcludeColVal);
				}
			}
		}
		catch (e)
		{
			if (e.message != undefined)
			{
				log.error('ERROR' , e.name + ' ' + e.message);
				throw e.name + ' ' + e.message;
			}
			else
			{
				log.error('ERROR', 'Unexpected Error' , e.toString()); 
				throw error.create({
					name: '99999',
					message: e.toString()
				});
			}
		} 
		finally 
		{
			log.debug(stLogTitle, ' <<-- Exit' );
		}
		
		return true;
		
	};
	
	function convNull(value)
	{
		if(value == null || value == undefined)
			value = '';
		return value;
	}
	
	return EntryPoint;
});


