/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @author Julius
 */
define(
['N/log', 'N/https', 'N/url', 'N/search', 'N/record', './moments.js'],

function (log, https, url, search, record, moments)
{
	var triggerSLCreator =
	{
	    execute: function (scriptContext)
    	{
	    	var stLogTitle = 'triggerSLCreator';
	    	log.debug(stLogTitle, 'START SCRIPT');

	    	var SCHED_DAILY = '1';
	    	var SCHED_WEEKLY = '2';
	    	var SCHED_MONTHLY = '3';
	    	var SCHED_MONTHLY_FISCAL = '4';
	    	
	    	var objDeploymentRecs = search.load({
	            type: 'scriptdeployment',
	            id: 'customsearch912'
	        }).run();
	
	        var objDeploymentRecsResult = objDeploymentRecs.getRange({
	            start: 0,
	            end: 1000
	        });
	
	        if (objDeploymentRecsResult)
	        {
	        	for (var idx in objDeploymentRecsResult)
	            {
	        		try
		        	{
		        		var stDeploymentId = objDeploymentRecsResult[idx].getValue('internalid');
		        		var stScriptId = objDeploymentRecsResult[idx].getValue('script');
		        		var stDeploymentTitle = objDeploymentRecsResult[idx].getValue('title');
		        		var stDeploymentCountId = record.load({
		        			type: 'scriptdeployment',
		                	id: stDeploymentId
		        		}).getValue('deploymentid');
		        		
		        		log.debug(stLogTitle, 'stDeploymentId = ' + stDeploymentId
		        				+ ', stScriptId = ' + stScriptId
		        				+ ', stDeploymentTitle = ' + stDeploymentTitle
		        				+ ', stDeploymentCountId = ' + stDeploymentCountId);
		        		
		        		var objSLCreatorRecs = search.create({
	                        type: 'customrecord_appwrap_suitelet_creator',
	                        columns: ['internalid'],
	                        filters: [
	                        {
	                            name: 'name',
	                            operator: 'is',
	                            values: [stDeploymentTitle]
	                        }]
	                    }).run();
		    	
		    	        var objSLCreatorRecsResult = objSLCreatorRecs.getRange({
		    	            start: 0,
		    	            end: 1000
		    	        });
		    	
		    	        var stSLCreatorId = '';
		    	        
		    	        if (objSLCreatorRecsResult)
		    	        {
		    	        	for (var idx1 in objSLCreatorRecsResult)
		    	        	{
		    	        		stSLCreatorId = objSLCreatorRecsResult[idx1].getValue('internalid');
		    	        		break;
		    	        	}
		    	        }
		        		
		        		log.debug(stLogTitle, 'stSLCreatorId = ' + stSLCreatorId);
		        		
		        		if (stSLCreatorId == '')
		        		{
		        			log.error(stLogTitle, 'There is no Suitelet Creator record matching the Deployment Title.');
		        			continue;
		        		}
		        		
		        		var objSLCreator = search.lookupFields({
		        			type: 'customrecord_appwrap_suitelet_creator',
		                	id: stSLCreatorId,
		                	columns: ['custrecord_suitelet_rs', 'custrecord_suitelet_prio']
		                });
		        		
		        		var stSchedule = objSLCreator.custrecord_suitelet_rs[0].value;
		        		log.debug(stLogTitle, 'stSchedule = ' + stSchedule);
		        		
		        		var dtNextRunDate = null;
		        		var dtLastRunDate = null;
		        		
		        		var objSLCreatorRunRecs = search.create({
	                        type: 'customrecord_search_run_details',
	                        columns: ['custrecord_srd_last_sc_run_ts',
	                                'internalid'],
	                        filters: [
	                        {
	                            name: 'custrecord_srd_srch',
	                            operator: 'anyof',
	                            values: [stSLCreatorId]
	                        },
	                        {
	                            name: 'custrecord_srd_is_latest',
	                            operator: 'is',
	                            values: 'T'
	                        }]
	                    }).run();
		    	
		    	        var objSLCreatorRunRecsResult = objSLCreatorRunRecs.getRange({
		    	            start: 0,
		    	            end: 1000
		    	        });
		    	
		    	        if (objSLCreatorRunRecsResult)
		    	        {
		    	        	for (var idx2 in objSLCreatorRunRecsResult)
		    	        	{
			    	        	var stLastRunDate = objSLCreatorRunRecsResult[idx2].getValue('custrecord_srd_last_sc_run_ts');
			    	        	log.debug(stLogTitle, 'stLastRunDate = ' + stLastRunDate);
			    	        	
			    	        	if (stLastRunDate != null && stLastRunDate != '')
			    	        	{
			    	        		dtLastRunDate = new Date(stLastRunDate);
			    	        	}
			    	        	
			    	        	break;
		    	        	}
		    	        }
		    	        
		    	        var bIsFirstRun = false;
		    	        if (dtLastRunDate == null)
		    	        {
		    	        	bIsFirstRun = true;
		    	        	dtLastRunDate = new Date();
		    	        }
		    	        
		    	        switch(stSchedule)
		    	        {
			    	        case SCHED_DAILY:
			    	        	dtNextRunDate = moments(dtLastRunDate).add(1, 'day');
			    	        	break;
			    	        case SCHED_WEEKLY:
			    	        	dtNextRunDate = moments(dtLastRunDate).add(7, 'day');
			    	        	break;
			    	        case SCHED_MONTHLY:
			    	        case SCHED_MONTHLY_FISCAL:
			    	        	dtNextRunDate = moments(dtLastRunDate).add(1, 'M');
			    	        	break;
			    	        default:
			    	        	break;
		    	        }
		    	        
		    	        dtNextRunDate = new Date(dtNextRunDate);
		    	        
		    	        log.debug(stLogTitle, 'dtNextRunDate = ' + dtNextRunDate
		    	        		+ ', dtLastRunDate = ' + dtLastRunDate
		    	        		+ ', bIsFirstRun = ' + bIsFirstRun);
		    	        
		    	        if ((dtNextRunDate == dtLastRunDate) || bIsFirstRun)
		    	        {
		    	        	// run suitelet
//		    	        	var stURL = url.resolveScript({
//		    	        	    scriptId: stScriptId,
//		    	        	    deploymentId: stDeploymentCountId,
//		    	        	    returnExternalUrl: false
//		    	        	});
		    	        	
		    	        	var stURL = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=' + stScriptId + '&deploy=' + stDeploymentCountId + '&compid=TSTDRV1460448&h=609671795ac82ae54cd4';
		    	        	
		    	        	var stPriority = objSLCreator.custrecord_suitelet_prio[0].value;
		    	        	log.debug(stLogTitle, 'stPriority = ' + stPriority);
		    	        	
		    	        	if (stPriority == '4')
		    	        	{
		    	        		stURL = stURL + '&custpage_send_email=send_email';
		    	        	}
		    	        	
		    	        	stURL = stURL + '&custpage_run_type=scheduled';
		    	        	log.debug(stLogTitle, 'stURL = ' + stURL);
		    	        	
		    	        	var response = https.get({
		    	        	    url: stURL
		    	        	});
		    	        	
		    	        	log.debug(stLogTitle, 'response = ' + response);
		    	        }
		            }
	        		catch (ex)
	        		{
	        			log.error(stLogTitle, ex.toString());
	        		}
	            }
	        }
	    	
	    	log.debug(stLogTitle, 'END SCRIPT');
    	}
    };
    
    return triggerSLCreator;
});
